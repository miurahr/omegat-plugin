/*===========================================================================
  Copyright (C) 2013-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterConfigurationMapper;

public class XLIFFOptions extends JDialog {

	protected static final ResourceBundle res = ResourceBundle.getBundle("omegat-okapi-plugin", Locale.getDefault());
	
	private static final long serialVersionUID = 1L;
	
	private final Map<String, String> options;
	final JRadioButton rdDefault;
	final JRadioButton rdCustom;
	final JTextField edPath;
	final JButton btGetPath;
	final JLabel pathNote;
	final JCheckBox chkIncludeTransUnitName;
	final JCheckBox chkProtectFinal;
	final JCheckBox chkProtectNoTrans;
	final JCheckBox chkUseNameAsId;

	// Internal class for the option actions
	class OptionsListener implements ActionListener {
		@Override
		public void actionPerformed (ActionEvent event) {
			edPath.setEnabled(rdCustom.isSelected());
			btGetPath.setEnabled(rdCustom.isSelected());
			if ( pathNote.getText().isEmpty() ) {
				pathNote.setText(" ");
			}
			
			if ( rdCustom.isSelected() ) {
				String path = options.get(AbstractOkapiFilter.USE_CUSTOM);
				if ( !Util.isEmpty(path) ) {
					if ( AbstractOkapiFilter.getIsProjectSpecific() ) {
						String projPath = AbstractOkapiFilter.getProjectPath();
						if ( projPath != null ) {
							if ( !path.contains("/") && !path.contains("\\") ) { // likely a new style path with just filename
								path = projPath + AbstractOkapiFilter.OMEGAT_CONFIG_FOLDER + "/" + path;
							}
						}
					}
					edPath.setText(path);
				}
			}
			else {
				edPath.setText("");
				pathNote.setText(" ");
			}
		}
	}
	
	/**
	 * Creates an XLIFFOptions object.
	 * @param parent the parent dialog.
	 * @param paramOptions the options.
	 */
	public XLIFFOptions (final Window parent,
		final Map<String, String> paramOptions,
		final String defaultConfigId)
	{
		super(parent, res.getString("xliffOptions.window.title"));
		setModal(true);
		
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		if ( paramOptions == null ) {
			this.options = new HashMap<String, String>();
		}
		else {
			this.options = paramOptions;
		}

		Container cp = getContentPane();
	    cp.setLayout(new GridBagLayout());

	    OptionsListener optListner = new OptionsListener();
	    
		rdDefault = new JRadioButton(String.format(res.getString("radioButton.Default"), defaultConfigId));
		rdDefault.addActionListener(optListner);
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.gridx = 0; c.gridwidth = 3;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(10, 10, 0, 10);
    	cp.add(rdDefault, c);

	rdCustom = new JRadioButton(res.getString("radioButton.Custom"));
		rdCustom.addActionListener(optListner);
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 3;
        c.gridy = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 10, 0, 10);
    	cp.add(rdCustom, c);
		
    	ButtonGroup grpOptions = new ButtonGroup();
    	grpOptions.add(rdDefault);
    	grpOptions.add(rdCustom);
		String path = options.get(AbstractOkapiFilter.USE_CUSTOM);
    	String def = options.get(AbstractOkapiFilter.USE_DEFAULT);
    	if (( def == null ) || !def.equals(AbstractOkapiFilter.VALUE_YES) ) {
    		if ( Util.isEmpty(path) ) rdDefault.setSelected(true);
    		else rdCustom.setSelected(true);
    	}
    	else {
    		rdDefault.setSelected(true);
    	}
    	
    	final JPanel pnlPath = new JPanel(new GridBagLayout());
    	
    	edPath = new JTextField();
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0;
        //c.insets = new Insets(0, 10, 5, 0);
    	pnlPath.add(edPath, c);

	    btGetPath = new JButton("...");
	    btGetPath.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		JFileChooser dlg = new JFileChooser();
	    		dlg.setDialogTitle(res.getString("fc.customFilter.title"));
	    		dlg.setFileFilter(new FileNameExtensionFilter(
	    			res.getString("fc.customFilter.fileFilter") + " (*"+FilterConfigurationMapper.CONFIGFILE_EXT+")",
	    			FilterConfigurationMapper.CONFIGFILE_EXT.substring(1)));
	    		String path = edPath.getText();
	    		if ( !Util.isEmpty(path) ) {
	    			dlg.setSelectedFile(new File(path));
	    		}
	    		if ( dlg.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
	    			String newPath = dlg.getSelectedFile().getAbsolutePath();
	    			edPath.setText(newPath);
	    			
	    			// If using project-specific settings, warn user that we'll copy their parameters file to the project folder 
					if ( AbstractOkapiFilter.getIsProjectSpecific() ) {
						String projPath = AbstractOkapiFilter.getProjectPath();
						if ( projPath != null ) {
							String paramFileDir = projPath + AbstractOkapiFilter.OMEGAT_CONFIG_FOLDER;
							String altSlashProjPath = projPath.replace("/", "\\");
							if ( !newPath.contains(projPath) && !newPath.contains(altSlashProjPath) ) {
								pathNote.setText(
									"<html><body style='width: 500px;'>"
									+ res.getString("xliffOptions.note.text")
									+ "<b>" + paramFileDir + "</b></body></html>"
								);
							}
							else {
								pathNote.setText(" ");
							}
						}
					}
	    		}
	    	}
	    });
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_END;
        c.gridx = 1;
        c.gridy = 0;
        c.insets = new Insets(0, 5, 0, 0);
	    pnlPath.add(btGetPath, c);
	    
	    pathNote = new JLabel(" ");
        pathNote.setForeground(Color.red);
	    c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0;
        c.gridwidth = 2;
        c.gridy = 1;
        c.fill = GridBagConstraints.NONE;
        c.weightx = 1.0;
	    pnlPath.add(pathNote, c);

	    c = new GridBagConstraints();
	    c.anchor = GridBagConstraints.LINE_START;
	    c.gridx = 0; c.gridwidth = 3;
	    c.gridy = 2;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 1.0;
	    c.insets = new Insets(0, 10, 5, 10);
	    cp.add(pnlPath, c);

    	final JPanel pnlOT = new JPanel(new GridBagLayout());

    	chkIncludeTransUnitName = new JCheckBox(res.getString("checkbox.IncludeTransUnitName"));
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 1;
        c.gridy = 0;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(10, 10, 0, 10);
    	pnlOT.add(chkIncludeTransUnitName, c);
    	
    	chkProtectNoTrans = new JCheckBox(res.getString("checkbox.ProtectNoTrans"));
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 1;
        c.gridy = 1;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 10, 0, 10);
    	pnlOT.add(chkProtectNoTrans, c);
    	
    	chkProtectFinal = new JCheckBox(res.getString("checkbox.ProtectFinal"));
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 1;
        c.gridy = 2;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 10, 0, 10);
    	pnlOT.add(chkProtectFinal, c);

    	chkUseNameAsId = new JCheckBox(res.getString("checkbox.UseNameAsId"));
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 1;
        c.gridy = 3;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(10, 10, 0, 10);
    	pnlOT.add(chkUseNameAsId, c);

	    c = new GridBagConstraints();
	    c.anchor = GridBagConstraints.LINE_START;
	    c.gridx = 0; c.gridwidth = 3;
	    c.gridy = 3;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 1.0;
	    c.insets = new Insets(0, 0, 20, 0);
        cp.add(pnlOT, c);

        String val = options.get(XLIFFFilter.PROTECT_FINAL);
        chkProtectFinal.setSelected(val==null || val.equals(XLIFFFilter.VALUE_YES));
        val = options.get(XLIFFFilter.INCLUDE_TU_NAME);
        chkIncludeTransUnitName.setSelected(val==null || val.equals(XLIFFFilter.VALUE_YES));
        val = options.get(XLIFFFilter.PROTECT_NOTRANS); 
        chkProtectNoTrans.setSelected(XLIFFFilter.VALUE_YES.equals(val));
        val = options.get(AbstractOkapiFilter.USE_NAME_AS_ID);
        chkUseNameAsId.setSelected(val==null || val.equals(XLIFFFilter.VALUE_YES));

    	// Do the actions for the options
    	optListner.actionPerformed(null);

    	final JPanel pnlAction = new JPanel(new GridLayout(1, 2, 5, 0));
	    
	    final JButton btOK = new JButton(res.getString("dialogButtons.okButton"));
	    btOK.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		// Set the default to yes if it's selected
	    		if ( rdDefault.isSelected() ) {
	    			paramOptions.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_YES);
	    		}
	    		// Then look at the path 
	    		if ( rdCustom.isSelected() ) {
		    		String path = edPath.getText();
	    			
	    			// Path must not be empty
	    			if ( Util.isEmpty(path) ) {
	    				JOptionPane.showMessageDialog(parent, res.getString("dialog.customFilter.msg"), res.getString("dialog.customFilter.title"), JOptionPane.ERROR_MESSAGE);
	    				edPath.requestFocusInWindow();
	    				return;
	    			}
	    			
	    			// The specified file must exist
					File sourceParamFile = new File(path);
	    			if ( !sourceParamFile.exists() ) {
	    				JOptionPane.showMessageDialog(parent, res.getString("dialog.paramFile.msg"), res.getString("dialog.paramFile.title"), JOptionPane.ERROR_MESSAGE);
	    				edPath.requestFocusInWindow();
	    				return;
	    			}

    	    		// Save the path (empty or not in all options)
	    			// If using project-specific settings, we copy the custom parameters file to the project folder
    				if ( AbstractOkapiFilter.getIsProjectSpecific() ) {
    					String projPath = AbstractOkapiFilter.getProjectPath();
        				if ( projPath == null ) {
        	    			// No path to the project folder (no project open?)
    	    				JOptionPane.showMessageDialog(parent, res.getString("dialog.noProject.msg"), res.getString("dialog.noProject.title"), JOptionPane.ERROR_MESSAGE);
    	    				edPath.requestFocusInWindow();
    	    				return;
        				}

    					File destParamDir = new File(projPath + AbstractOkapiFilter.OMEGAT_CONFIG_FOLDER);
    					File destParamFile = new File(destParamDir + "/"+ sourceParamFile.getName());

    					// Make sure we are not trying to copy the same file to itself
    					if ( !sourceParamFile.getAbsolutePath().equals(destParamFile.getAbsolutePath()) ) {
    						try {
            					if ( !destParamDir.exists() ) { destParamDir.mkdir(); }
        						Files.copy(sourceParamFile.toPath(), destParamFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        					} 
        					catch (IOException err) {
        						err.printStackTrace();
        					}
        					
            	    		if ( destParamFile.exists() ) {
            	    			// Copy was successful, store the filename instead of full path
                				paramOptions.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_NO);
            	    			paramOptions.put(AbstractOkapiFilter.USE_CUSTOM, destParamFile.getName());
            	    		}
            	    		else {
            	    			// There was a problem copying the file
        	    				JOptionPane.showMessageDialog(parent, res.getString("dialog.copyError.msg"), res.getString("dialog.copyError.title"), JOptionPane.ERROR_MESSAGE);
        	    				edPath.requestFocusInWindow();
        	    				return;
            	    		}
    					}
    					else {
    						// Source file is equal to destination file, no copy needed
            				paramOptions.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_NO);
        	    			paramOptions.put(AbstractOkapiFilter.USE_CUSTOM, sourceParamFile.getName());
    					}
    				}
    	    		else {
    	    			// Not using project specific settings, no copy needed, and we store the full path to the file
        				paramOptions.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_NO);
    	    			paramOptions.put(AbstractOkapiFilter.USE_CUSTOM, path);
    	    		}
	    		}

    			// OmegaT behavior
   				paramOptions.put(AbstractOkapiFilter.INCLUDE_TU_NAME,
					chkIncludeTransUnitName.isSelected() ? AbstractOkapiFilter.VALUE_YES : AbstractOkapiFilter.VALUE_NO);
   				paramOptions.put(AbstractOkapiFilter.PROTECT_NOTRANS,
   					chkProtectNoTrans.isSelected() ? AbstractOkapiFilter.VALUE_YES : AbstractOkapiFilter.VALUE_NO);
   				paramOptions.put(AbstractOkapiFilter.PROTECT_FINAL,
   					chkProtectFinal.isSelected() ? AbstractOkapiFilter.VALUE_YES : AbstractOkapiFilter.VALUE_NO);
   				paramOptions.put(AbstractOkapiFilter.USE_NAME_AS_ID,
					chkUseNameAsId.isSelected() ? XLIFFFilter.VALUE_YES : XLIFFFilter.VALUE_NO);

   				dispose();
	    	}
	    });
	    pnlAction.add(btOK);
        getRootPane().setDefaultButton(btOK);

        @SuppressWarnings("serial")
        AbstractAction cancelAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        };
        // Allow canceling the dialog with the Esc key
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                "ESC_CLOSE");
        getRootPane().getActionMap().put("ESC_CLOSE", cancelAction);

    	final JButton btCancel = new JButton(res.getString("dialogButtons.cancelButton"));
	    btCancel.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		dispose();
	    	}
	    });
	    pnlAction.add(btCancel);
	    
	    c = new GridBagConstraints();
	    c.anchor = GridBagConstraints.LINE_END;
	    c.gridx = 0; c.gridwidth = 3;
	    c.gridy = 4;
	    c.insets = new Insets(0, 10, 10, 10);
	    cp.add(pnlAction, c);

	    pack();
	    setMinimumSize(new Dimension(600, getSize().height));
	    setMaximumSize(new Dimension(600, getSize().height));

	    // Center the dialog
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((dim.width-getSize().width)/2, (dim.height-getSize().height)/2);

        // Set the focus properly
    	if ( rdDefault.isSelected() ) rdDefault.requestFocusInWindow();
    	else rdCustom.requestFocusInWindow();
	}
	
	/**
	 * Gets the options.
	 * @return a map of option key-value pairs, never null.
	 */
	public Map<String, String> getOptions () {
		return options;
	}
}
