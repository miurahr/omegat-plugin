package net.sf.okapi.lib.omegat;

import java.util.Locale;
import java.util.ResourceBundle;

import org.omegat.filters2.Instance;

public class MarkdownFilter extends AbstractOkapiFilter {

    private static final String EXTENSION = ".md";
    
    protected static final ResourceBundle res = ResourceBundle.getBundle("omegat-okapi-plugin", Locale.getDefault());

    public MarkdownFilter () {
        initialize(res.getString("markdownFilter.name"),
            "okf_markdown",
            EXTENSION);
    }

    @Override
    public Instance[] getDefaultInstances () {
        return new Instance[] {
            new Instance("*"+EXTENSION)
        };
    }

}
