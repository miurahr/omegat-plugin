/*===========================================================================
  Copyright (C) 2013 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.awt.Dialog;
import java.awt.Window;
import java.io.File;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.omegat.filters2.FilterContext;
import org.omegat.filters2.Instance;

public class XLIFFFilter extends AbstractOkapiFilter {

	private static final String EXTENSION = ".xlf;*.xliff;*.mxliff;*.mqxliff";
	
	protected static final ResourceBundle res = ResourceBundle.getBundle("omegat-okapi-plugin", Locale.getDefault());
	
	public XLIFFFilter () {
		initialize(res.getString("xliffFilter.name"),
			"okf_xliff",
			EXTENSION);
	}

	@Override
	public Instance[] getDefaultInstances () {
        return new Instance[] {
        	new Instance("*.xlf"),
        	new Instance("*.xliff"),
        	new Instance("*.mxliff"),
        	new Instance("*.mqxliff")
        };
	}

	@Override
	public boolean isFileSupported (File inFile,
		Map<String, String> config,
		FilterContext context)
	{
		// Extension check is already done by OmegaT
		// Here we just need to check cases when an extension could be used for
		// several different filters (like .xliff)
		try {
			String version = XLIFFDetector.getXLIFFVersion(inFile);
			return version.startsWith("1.");
		}
		catch ( Throwable e ) {
			return false;
		}
	}

	@Override
	public Map<String, String> changeOptions (Dialog parent,
		Map<String, String> config)
	{
        return changeOptions((Window) parent, config);
	}
	
	@Override
	public Map<String, String> changeOptions (Window parent,
		Map<String, String> config)
	{
		XLIFFOptions dlg = new XLIFFOptions(parent, config, filterConfigId);
		dlg.setVisible(true);
		return dlg.getOptions();
	}

    @Override
    public boolean isBilingual() {
        return true;
    }
}
