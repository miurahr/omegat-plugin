/*===========================================================================
  Copyright (C) 2010-2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.lib.omegat;

import org.omegat.core.Core;

public class OmegatOkapiFilter {
    @SuppressWarnings("unused")
    public static void loadPlugins() {
        Core.registerFilterClass(DoxygenFilter.class);
        Core.registerFilterClass(HTMLFilter.class);
        Core.registerFilterClass(IDMLFilter.class);
        Core.registerFilterClass(JSONFilter.class);
        Core.registerFilterClass(MarkdownFilter.class);
        Core.registerFilterClass(OpenXMLFilter.class);
        Core.registerFilterClass(SDLXLIFFFilter.class);
        Core.registerFilterClass(TransifexFilter.class);
        Core.registerFilterClass(TSFilter.class);
        Core.registerFilterClass(TTXFilter.class);
        Core.registerFilterClass(TXMLFilter.class);
        Core.registerFilterClass(XLIFF2Filter.class);
        Core.registerFilterClass(XLIFFFilter.class);
        Core.registerFilterClass(XMLFilter.class);
        Core.registerFilterClass(XMLStreamFilter.class);
        Core.registerFilterClass(YAMLFilter.class);
    }

    @SuppressWarnings("unused")
    public static void unloadPlugins() {
    }


}
