package net.sf.okapi.lib.omegat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Utils {

	/**
	 * Checks if a given text is in a given file.
	 * @param file the file to check.
	 * @param text the text to search for (no line-breaks).
	 * @return true if found, false if not.
	 * @throws IOException 
	 */
	static public boolean findInFile (File file,
		String text)
		throws IOException
	{
		try ( BufferedReader br = new BufferedReader(new FileReader(file)) ) {
			String line;
			while ( (line = br.readLine()) != null ) {
				if ( line.indexOf(text) != -1 ) return true;
			}
		}
		
		return false;
	}

}
