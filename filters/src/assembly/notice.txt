The Okapi libraries are under Apache 2.0 License.

You can find the source code of the libraries at:
https://bitbucket.org/okapiframework/omegat-plugin

For details and question about the Okapi Framework, please go to:
http://okapiframework.org/
